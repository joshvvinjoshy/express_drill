// const express = require('express')
// const path = require('path')
// const app = express()
// const logger = require('./middleware/logger.js')
// const port = process.env.PORT || 8000
// app.use(express.json())
// app.use(express.urlencoded({extended : false}))
// app.use(express.static(path.join(__dirname,'public')))

// // app.use(logger)
// app.use('/api/members', require('./routes/api/members.js'))
// app.listen(port, ()=>{
//     console.log(`Server started listening on ${port}`)
// })
const express = require('express')
const app = express()
const path = require('path')
const uuid = require('uuid')
// const {readFile} = require('fs').promises
const port = process.env.PORT || 8000
app.get('/html', (request,response)=>{
    response.sendFile(__dirname + '/public/hello.html')
})
app.get('/json', (request,response)=>{
    response.sendFile(__dirname + '/jsondata.json')
})
app.get('/uuid', (request,response)=>{
    const data_to_send = {
        "uuid": uuid.v4()
    }
    response.send(data_to_send)
})
app.get('/status/:st', (request,response)=>{
    response.status(parseInt(request.params.st))
    response.send(request.params.st)
})
app.get('/delay/:d', (request,response)=>{
    setTimeout(()=>{
        response.send(`Got the message after ${request.params.d} seconds`) 
    }, parseInt(request.params.d)*1000)
})
app.listen(port, ()=>{
    console.log(`Server started listening on ${port}`)
})
